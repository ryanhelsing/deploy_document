DigitalOcean Ubuntu 14.04 x64

dnsimple
blank A record to domain

ssh root@[IP]
sudo adduser deployer
sudo adduser deployer sudo 

vim /etc/ssh/sshd_config
Port 2623 # change this to whatever port you wish to use
Protocol 2
PermitRootLogin no

ADD:
UseDNS no
AllowUsers deployer

reload ssh


su deployer

add local key to .ssh/authorized_keys

pbcopy < ~/.ssh/id_rsa.pub

sudo apt-get install curl git-core nginx -y

sudo apt-get update
curl -sSL https://get.rvm.io | bash -s stable
source ~/.rvm/scripts/rvm
rvm requirements
rvm install 2.1.0
rvm use 2.1.0 --default
rvm rubygems current
gem install rails --no-ri --no-rdoc -V
gem install bundler --no-ri --no-rdoc -V
sudo apt-get install nodejs

gpg --keyserver keyserver.ubuntu.com --recv-keys 561F9B9CAC40B2F7
gpg --armor --export 561F9B9CAC40B2F7 | sudo apt-key add -
sudo apt-get install apt-transport-https
sudo sh -c "echo 'deb https://oss-binaries.phusionpassenger.com/apt/passenger trusty main' >> /etc/apt/sources.list.d/passenger.list"
sudo chown root: /etc/apt/sources.list.d/passenger.list
sudo chmod 600 /etc/apt/sources.list.d/passenger.list
sudo apt-get update
sudo apt-get install nginx-full passenger
sudo service nginx start

sudo vim /etc/nginx/nginx.conf
#uncomment and EDIT the line beginning passenger_root and passenger_ruby
passenger_root /usr/lib/ruby/vendor_ruby/phusion_passenger/locations.ini;
passenger_ruby /home/deployer/.rbenv/shims/ruby;

sudo vim /etc/nginx/sites-enabled/default
server {
  listen 80;
  server_name centerian.com;
  passenger_enabled on;
  root /home/deployer/app_name/current/public;
}

sudo service nginx restart

sudo apt-get install postgresql postgresql-contrib
sudo su - postgres
psql
ALTER USER postgres WITH PASSWORD '<newpassword>';
create database symbiotic_home_production;
\quit
sudo apt-get install libpq-dev
sudo apt-get install bundler
sudo apt-get install nodejs


gem 'mina'
mina init

set :rails_env, 'production'
set :domain, '104.236.28.199'
set :deploy_to, '/home/deployer/symbiotic_home'
set :repository, 'git@bitbucket.org:ryanhelsing/symbiotic_home.git'
set :branch, 'master'
set :user, 'deployer'
set :password, 'Sittingkills9'
set :forward_agent, true
set :port, '22'
set :term_mode, nil

# uncomment rbenv
invoke :'passenger:restart' # inside launch


set :shared_paths, ['config/database.yml', 'log', 'config/secrets.yml']

# in setup:
queue! %[touch "#{deploy_to}/shared/config/secrets.yml"]
queue %[echo "-----> Be sure to edit 'shared/config/secrets.yml'."]
# NEEDS SECRET KEY IN ADDITION TO DATABASE

# add to end of file:
desc "Restarts the nginx server."
task :restart do
  invoke :'passenger:restart'
end

namespace :passenger do
  task :restart do
    queue "mkdir #{deploy_to}/current/tmp; touch #{deploy_to}/current/tmp/restart.txt"
  end
end

mina setup
#setup database credentials on server
rake db:create db:migrate #on server after first failed deploy
mina deploy

#MINA FIX
in current director,
gem install bundler
bundle install --no-deployment

#add to mina fix
task :fix do
  queue "cd symbiotic_home/current"
  queue "bundle install --no-deployment"
end

#should probably make as part of initial deploy process

#Build Swap File!
sudo dd if=/dev/zero of=/swapfile bs=1024 count=256k
sudo mkswap /swapfile
sudo swapon /swapfile
swapon -s # to see

sudo vim /etc/fstab
# add to file:
 /swapfile       none    swap    sw      0       0  

#run:
echo 10 | sudo tee /proc/sys/vm/swappiness
echo vm.swappiness = 10 | sudo tee -a /etc/sysctl.conf
sudo chown root:root /swapfile 
sudo chmod 0600 /swapfile
